<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class registerController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(){
        return view('welcome');
    }

    public function welcome_post(Request $r){
        $firstname = $r->fname;
        $lastname = $r->lname;
        return view('welcome' , compact('firstname' , 'lastname'));
    }
}
