<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="fname"> <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name="lname"> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="gen">Male <br>
        <input type="radio" name="gen">Female <br>
        <input type="radio" name="gen">Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="National">
            <option value="Indonesian">Indonesian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Singapore">Singapore</option>
            <option value="Thailand">Thailand</option>
        </select> <br><br>
        <label>language Spoken:</label> <br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>